﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;

namespace OnlineAuctionSystem.DataAccessLibrary
{
    public class BaseDataAccess
    {
        private readonly int _timeoutSeconds;
        private readonly string _connectionString;

        public BaseDataAccess(IDatabaseConfig database)
        {
            _connectionString = database.DatabaseConfigurationString;
            _timeoutSeconds = database.Timeout;
        }

        public async Task<IEnumerable<T>> ExecuteStoredProcedureWithCollectionResultAsync<T>(string procedure,
            DynamicParameters parameters = null)
        {
            using (var cts = new CancellationTokenSource(_timeoutSeconds * 1000))
            {
                var cd = new CommandDefinition(procedure, parameters, commandType: CommandType.StoredProcedure, cancellationToken: cts.Token, commandTimeout: _timeoutSeconds);
                using (var connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync(cts.Token).ConfigureAwait(false);
                    try
                    {
                        return await connection.QueryAsync<T>(cd).ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Exception in {procedure}", ex);
                    }
                }
            }
        }

        public async Task ExecuteVoidStoredProcedureAsync(string procedure, DynamicParameters parameters = null)
        {
            using (var cts = new CancellationTokenSource(_timeoutSeconds * 1000))
            {
                var cd = new CommandDefinition(procedure, parameters, commandType: CommandType.StoredProcedure, cancellationToken: cts.Token, commandTimeout: _timeoutSeconds);
                using (var connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync(cts.Token).ConfigureAwait(false);
                    try
                    {
                        await connection.ExecuteAsync(cd).ConfigureAwait(false);
                    }
                    catch (SqlException ex)
                    {
                        throw new Exception($"Exception in {procedure}", ex);
                    }
                }
            }
        }

        public async Task<T> ExecuteStoredProcedureWithSingleResultAsync<T>(string procedure,
            DynamicParameters parameters = null)
        {
            var result = await ExecuteStoredProcedureWithCollectionResultAsync<T>(procedure, parameters)
                .ConfigureAwait(false);
            return result.FirstOrDefault();
        }
    }
}