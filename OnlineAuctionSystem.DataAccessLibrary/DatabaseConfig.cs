﻿namespace OnlineAuctionSystem.DataAccessLibrary
{
    public interface IDatabaseConfig
    {
        public string DatabaseConfigurationString { get; }
        public int Timeout { get; }
    };
}